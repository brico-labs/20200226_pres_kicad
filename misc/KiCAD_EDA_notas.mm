<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="KiCAD EDA" FOLDED="false" ID="ID_1607997894" CREATED="1582634791465" MODIFIED="1582634815901" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="Qu&#xe9; es?" POSITION="right" ID="ID_245221365" CREATED="1582634830730" MODIFIED="1582634840054">
<edge COLOR="#ff0000"/>
<node TEXT="Open Source Software Suite" ID="ID_1022799657" CREATED="1582634864685" MODIFIED="1582634874817">
<node TEXT="Suite: Un conjunto de programas" ID="ID_721560667" CREATED="1582634877270" MODIFIED="1582634884294"/>
<node TEXT="GNU GPL V3" ID="ID_1273752417" CREATED="1582634904390" MODIFIED="1582634908693"/>
<node TEXT="Multiplataforma" ID="ID_1100218837" CREATED="1582634976509" MODIFIED="1582634981313"/>
</node>
<node TEXT="Funciones" ID="ID_432632938" CREATED="1582634915117" MODIFIED="1582634917397">
<node TEXT="Captura de esquemas de circuitos electr&#xf3;nicos" ID="ID_529030867" CREATED="1582634923182" MODIFIED="1582634940802"/>
<node TEXT="Generaci&#xf3;n de PCB" ID="ID_1399209018" CREATED="1582634941081" MODIFIED="1582634958238"/>
<node TEXT="Salida en formato Gerber" ID="ID_459792685" CREATED="1582634958685" MODIFIED="1582634969633"/>
</node>
</node>
<node TEXT="Historia" POSITION="right" ID="ID_313145323" CREATED="1582634988546" MODIFIED="1582634991449">
<edge COLOR="#0000ff"/>
<node TEXT="Creado en 1992 por Jean Pierre Charras" ID="ID_221723064" CREATED="1582634993681" MODIFIED="1582635018157"/>
<node TEXT="En 2007 se hace p&#xfa;blico el c&#xf3;digo" ID="ID_1083073554" CREATED="1582635019065" MODIFIED="1582635072865">
<node TEXT="SourceForge" ID="ID_1920490699" CREATED="1582635076257" MODIFIED="1582635081401"/>
<node TEXT="Tres desarrolladores" ID="ID_196939371" CREATED="1582635081969" MODIFIED="1582635086346"/>
</node>
<node TEXT="Del 2007 al 2012" ID="ID_1028904930" CREATED="1582635123481" MODIFIED="1582635151413">
<node TEXT="Documentaci&#xf3;n en Doxygen" ID="ID_1362206960" CREATED="1582635086845" MODIFIED="1582635098185"/>
<node TEXT="Re-facturaci&#xf3;n total" ID="ID_407305855" CREATED="1582635098501" MODIFIED="1582635108733"/>
<node TEXT="Re-documentaci&#xf3;n total" ID="ID_1368091647" CREATED="1582635109145" MODIFIED="1582635114280"/>
<node TEXT="Nuevas funcionalidades" ID="ID_790090447" CREATED="1582635114717" MODIFIED="1582635774399"/>
</node>
<node TEXT="2011" OBJECT="java.lang.Long|2011" ID="ID_1614941140" CREATED="1582635158669" MODIFIED="1582635164690">
<node TEXT="CERN lanza la Open Hardware Initiative" ID="ID_6883531" CREATED="1582635167700" MODIFIED="1582635180473">
<node TEXT="Objetivos" ID="ID_491140402" CREATED="1582635184581" MODIFIED="1582635188934"/>
<node TEXT="Manifesto" ID="ID_916912259" CREATED="1582635189401" MODIFIED="1582635192133"/>
</node>
</node>
<node TEXT="2012" OBJECT="java.lang.Long|2012" ID="ID_1231573580" CREATED="1582635195557" MODIFIED="1582635198117">
<node TEXT="El CERN escoje KiCAD como su software oficial para desarrollo de circuitos" ID="ID_1569008618" CREATED="1582635200109" MODIFIED="1582635221411"/>
</node>
<node TEXT="2012 a la actualidad" ID="ID_260649129" CREATED="1582635224116" MODIFIED="1582635242557">
<node TEXT="Fuerte impulso al proyecto" ID="ID_671936742" CREATED="1582635245321" MODIFIED="1582635258009"/>
<node TEXT="Se consolida cada vez m&#xe1;s el software" ID="ID_1038255426" CREATED="1582635258293" MODIFIED="1582635269476"/>
<node TEXT="Se unifican interfaces de usuario" ID="ID_297260493" CREATED="1582635270061" MODIFIED="1582635279200"/>
<node TEXT="Nuevas funciones" ID="ID_668662416" CREATED="1582635279769" MODIFIED="1582635294358">
<node TEXT="Importaciones de KiCAD" ID="ID_780401631" CREATED="1582635310897" MODIFIED="1582635321905"/>
<node TEXT="Uso de routers de terceras partes" ID="ID_1208200185" CREATED="1582635323737" MODIFIED="1582635334552"/>
<node TEXT="Geometr&#xed;as comunes" ID="ID_1675133205" CREATED="1582635336317" MODIFIED="1582635342080"/>
<node TEXT="Facilidades de enrutamiento" ID="ID_1904219442" CREATED="1582635342981" MODIFIED="1582635348232"/>
<node TEXT="Interfaz gr&#xe1;fico basado en openGL" ID="ID_1184717415" CREATED="1582635348513" MODIFIED="1582635362708"/>
</node>
</node>
<node TEXT="Futuro" ID="ID_470890759" CREATED="1582635538593" MODIFIED="1582635540252">
<node TEXT="Funciones de simulador" ID="ID_1522114715" CREATED="1582635542189" MODIFIED="1582635555512"/>
</node>
</node>
<node TEXT="Suite" POSITION="right" ID="ID_281998400" CREATED="1582635372101" MODIFIED="1582635374333">
<edge COLOR="#00ff00"/>
<node TEXT="Schematic Layout Editor" ID="ID_1983924500" CREATED="1582635385356" MODIFIED="1582635393040">
<node TEXT="Aqu&#xed; es donde creamos el esquema del circuito" ID="ID_871336298" CREATED="1582635575856" MODIFIED="1582635587544"/>
</node>
<node TEXT="Symbol Editor" ID="ID_746659314" CREATED="1582635393397" MODIFIED="1582635405105">
<node TEXT="Aqui es donde editamos o creamos nuevos s&#xed;mbolos para los esquemas de circuitos" ID="ID_683148026" CREATED="1582635591036" MODIFIED="1582635617777"/>
</node>
<node TEXT="PCB Layout Editor" ID="ID_288455375" CREATED="1582635418065" MODIFIED="1582635423404">
<node TEXT="Aqu&#xed; es donde creamos el PCB de nuestro circuito" ID="ID_1393663723" CREATED="1582635620840" MODIFIED="1582635635256"/>
</node>
<node TEXT="Footprint Editor" ID="ID_102685585" CREATED="1582635427669" MODIFIED="1582635433196">
<node TEXT="Aqui podemos editar o crear las &quot;huellas&quot; de nuestros componentes" ID="ID_84378385" CREATED="1582635638973" MODIFIED="1582635657872"/>
</node>
<node TEXT="Gerber Viewer" ID="ID_964351353" CREATED="1582635438769" MODIFIED="1582635441916">
<node TEXT="Aqu&#xed; se visualizan los ficheros Gerber de salida" ID="ID_1137943015" CREATED="1582635663624" MODIFIED="1582635674357"/>
</node>
<node TEXT="Bitmap to component converter" ID="ID_743083276" CREATED="1582635463257" MODIFIED="1582635470088">
<node TEXT="Important&#xed;sima funci&#xf3;n!" ID="ID_300604485" CREATED="1582635936832" MODIFIED="1582635947175"/>
</node>
<node TEXT="PCB Calculator" ID="ID_627547084" CREATED="1582635481081" MODIFIED="1582635485964">
<node TEXT="Reguladores" ID="ID_649552968" CREATED="1582635952495" MODIFIED="1582635957540"/>
<node TEXT="Anchos de pista" ID="ID_675174439" CREATED="1582635958003" MODIFIED="1582635960799"/>
<node TEXT="Espaciados" ID="ID_233961641" CREATED="1582635961319" MODIFIED="1582635989708"/>
<node TEXT="Guiaondas" ID="ID_1418482557" CREATED="1582635993495" MODIFIED="1582635996338"/>
<node TEXT="Calculo de atenuadores" ID="ID_1065094566" CREATED="1582635997200" MODIFIED="1582644228879"/>
</node>
<node TEXT="Page Layout Editor" ID="ID_667585559" CREATED="1582635510401" MODIFIED="1582635516408">
<node TEXT="Para editar el cajetin de los esquemas" ID="ID_1855012976" CREATED="1582644233452" MODIFIED="1582644245234"/>
</node>
</node>
<node TEXT="Referencias" POSITION="right" ID="ID_1021110285" CREATED="1582635737884" MODIFIED="1582635743480">
<edge COLOR="#ff00ff"/>
<node TEXT="KiCAD web site https://kicad-pcb.org/" ID="ID_260164118" CREATED="1582644278031" MODIFIED="1582644284715"/>
<node TEXT="KiCAS librerias de terceros" ID="ID_459776620" CREATED="1582645174168" MODIFIED="1582645184467">
<node TEXT="https://kicad-pcb.org/libraries/third_party/" ID="ID_1002475352" CREATED="1582645186084" MODIFIED="1582645187539"/>
<node TEXT="Digikey" ID="ID_443210332" CREATED="1582645188036" MODIFIED="1582645192567">
<node TEXT="https://github.com/Digi-Key/digikey-kicad-library" ID="ID_1711583642" CREATED="1582645597031" MODIFIED="1582645598455"/>
</node>
<node TEXT="SparkFun" ID="ID_1194549754" CREATED="1582645192980" MODIFIED="1582645198134">
<node TEXT="https://github.com/sparkfun/SparkFun-KiCad-Libraries" ID="ID_928212727" CREATED="1582645644195" MODIFIED="1582645644883"/>
</node>
<node TEXT="SnapEda" ID="ID_1644154462" CREATED="1582645198414" MODIFIED="1582645203056">
<node TEXT="https://www.snapeda.com/kicad/" ID="ID_548919370" CREATED="1582645696027" MODIFIED="1582645696796"/>
</node>
</node>
<node TEXT="Freerouting https://freerouting.org/" ID="ID_1127010778" CREATED="1582644285676" MODIFIED="1582644511507"/>
<node TEXT="Alhambra" ID="ID_1902576232" CREATED="1582644543984" MODIFIED="1582644547286">
<node TEXT="https://alhambrabits.com/" ID="ID_1744406468" CREATED="1582644548427" MODIFIED="1582644549519"/>
</node>
<node TEXT="FPGAWARS" ID="ID_1410526659" CREATED="1582644609467" MODIFIED="1582644615508">
<node TEXT="https://fpgawars.github.io/" ID="ID_547328439" CREATED="1582644616887" MODIFIED="1582644617716"/>
</node>
<node TEXT="ICEStudio" ID="ID_612649285" CREATED="1582644630530" MODIFIED="1582644636451">
<node TEXT="https://icestudio.io/" ID="ID_247996330" CREATED="1582644637590" MODIFIED="1582644638416"/>
</node>
<node TEXT="RISC-V en FPGA" ID="ID_457344199" CREATED="1582644659700" MODIFIED="1582644666051">
<node TEXT="https://github.com/Obijuan/RISC-V-FPGA" ID="ID_1473059714" CREATED="1582644668500" MODIFIED="1582644669659"/>
</node>
<node TEXT="Resumen RISC-V en FPGA" ID="ID_1738520204" CREATED="1582644683875" MODIFIED="1582644696470">
<node TEXT="https://hackaday.com/wp-content/uploads/2020/01/Megan-Wachs-RISC-V-and-FPGAs_-Open-Source-Hardware-Hacking.pdf" ID="ID_1135467803" CREATED="1582644698967" MODIFIED="1582644700351"/>
</node>
<node TEXT="RISC-V" ID="ID_1029520866" CREATED="1582644730019" MODIFIED="1582644734847">
<node TEXT="https://en.wikipedia.org/wiki/RISC-V" ID="ID_948091721" CREATED="1582644734848" MODIFIED="1582644736287"/>
</node>
<node TEXT="CERN Open Hardware Iniative" ID="ID_1076691351" CREATED="1582644765860" MODIFIED="1582644783791">
<node TEXT="https://home.cern/news/press-release/cern/cern-launches-open-hardware-initiative" ID="ID_1903382916" CREATED="1582644785596" MODIFIED="1582647174112" LINK="https://home.cern/news/press-release/cern/cern-launches-open-hardware-initiative"/>
<node TEXT="Licencia CERN https://www.ohwr.org/cernohl" ID="ID_630285392" CREATED="1582644881577" MODIFIED="1582644887613"/>
</node>
<node TEXT="Historia breve Open Hardware" ID="ID_1478432917" CREATED="1582644951942" MODIFIED="1582644962002">
<node TEXT="https://www.oshwa.org/research/brief-history-of-open-source-hardware-organizations-and-definitions/" ID="ID_620030072" CREATED="1582644963014" MODIFIED="1582644963914"/>
</node>
<node TEXT="CERN Open Hardware Manifesto" ID="ID_266884079" CREATED="1582645033018" MODIFIED="1582645042435">
<node TEXT="https://ohwr.org/project/ohr-support/wikis/Manifesto" ID="ID_604717100" CREATED="1582645043687" MODIFIED="1582645044422"/>
</node>
</node>
</node>
</map>
